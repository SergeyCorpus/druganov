$(function () {
	// Mobile menu
	$(".nav").click(function (e) {
		e.preventDefault();
		$(this).toggleClass("nav-expanded");
	});

	// slick carousel
	$(".rev_slider-js").slick({
		dots: true,
		arrows: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		focusOnSelect: true,
		infinite: true,
		centerMode: true,
		responsive: [
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
				},
			},
		],
	});

	$(".slick-cloned a").removeAttr("data-fancybox");
});




	/*открытие модального окна обратный звонок*/
	//open popup
	$('.cd-popup-trigger-callorder-js').on('click', function(event){
		event.preventDefault();
		$('#popup-callorder-js').addClass('is-visible');
		$("body").addClass('is-not-scroll');
	});

		/*открытие модального окна получить консультацию*/
	//open popup
	$('.cd-popup-trigger-consult-js').on('click', function(event){
		event.preventDefault();
		$('#popup-consult-js').addClass('is-visible');
		$("body").addClass('is-not-scroll');
	});


			/*открытие модального окна заказать проект*/
	//open popup
	$('.cd-popup-trigger-order-js').on('click', function(event){
		event.preventDefault();
		$('#popup-order-js').addClass('is-visible');
		$("body").addClass('is-not-scroll');
	});
	
	//close popup
	$('.cd-popup').on('click', function(event){
		if( $(event.target).is('.cd-popup-close') || $(event.target).is('.simplebar-content,.col-12,.row') ) {
			event.preventDefault();
			$(this).removeClass('is-visible');
			$("body").removeClass('is-not-scroll');
		}
	});
	//close popup when clicking the esc keyboard button
	$(document).keyup(function(event){
		if(event.which=='27'){
			$('.cd-popup').removeClass('is-visible');
		}
	});



	//настройки маски 
	$(function($) {
		$.mask.definitions['~']='[+-]';
		$('input[type="tel"]').mask('+7(999)999-99-99');
	});




// video iframe
(function () {
	var youtube = document.querySelectorAll(".youtube");
	for (var i = 0; i < youtube.length; i++) {
		var source = "https://img.youtube.com/vi/" + youtube[i].dataset.embed + "/sddefault.jpg";
		var image = new Image();
		image.src = source;
		image.addEventListener(
			"load",
			(function () {
				youtube[i].appendChild(image);
			})(i),
		);

		youtube[i].addEventListener("click", function () {
			var iframe = document.createElement("iframe");
			iframe.setAttribute("frameborder", "0");
			iframe.setAttribute("allowfullscreen", "");
			iframe.setAttribute("src", "https://www.youtube.com/embed/" + this.dataset.embed + "?rel=0&showinfo=0&autoplay=1");
			this.innerHTML = "";
			this.appendChild(iframe);
		});
	}
})();
